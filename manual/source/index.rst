.. Data Analysis Workflow documentation master file, created by
   sphinx-quickstart on Wed Nov 20 14:25:24 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Data Analysis Workflow
======================

Intro
-----

Working with scientific data, you will have to deal with data and files of 4
different categories:

* **Raw data**: Direct output of your measuring device.
* **Scripts**: Programs you write to process raw data and produce results.
* **Library**: Collection of recurring source code, parts of code you use in
  multiple scripts.
* **Processed data / results**: The actual goal of the whole trouble.

I can't give you a general rule how to organize these different parts of your project,
and you probably have your own system that you like. But I want to present a few
suggestions and what you should consider if you want to automate your analyses.

The goal is a mostly automatic and quick analysis. It should be flexible enough, that if you add
new raw data, you only have to restart one script and the analysis is repeatet with the new data
included.

I prepared an example project folder that you can
:download:`download here </_static/project_xy.zip>` .
The layout of the project folder is outlined in :ref:`ExampleFolder`.

In :ref:`FindingFiles` I explain how to find files programatically. Once you know how to find
your raw data files in python, you should continue reading on :ref:`SaveResults`.

If you have computationally expensive analyses you run for many separate files, you can learn how
to run them in parallel in the section :ref:`Multiprocessing`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   example_folder
   find_files
   save_results
   multiprocessing


Links
-----

* :ref:`search`
