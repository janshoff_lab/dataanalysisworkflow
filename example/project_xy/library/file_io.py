import glob
import os
import pandas as pd


def find_traj_files(path_to_data):
    files = sorted(glob.glob(path_to_data + '/**/traj*.dat', recursive=True))
    return files

def load_index(path_to_data):
    fname_index = os.path.join(path_to_data, 'index.csv')
    index = pd.read_csv(fname_index, header=0, index_col=0)
    index['file'] = path_to_data + '/' + index['file']
    index['date'] = pd.to_datetime(index['date'])
    return index
