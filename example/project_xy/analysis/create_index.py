import os
import json
import pandas as pd
from lib_xy import file_io


def create_index(dir_data):
    df = pd.DataFrame(columns=['file', 'date', 'species', 'temperature', 'viscosity'])
    files_traj = file_io.find_traj_files(dir_data)
    for i, f in enumerate(files_traj):
        params = get_parameters_of_traj_file(f)
        df.loc[i] = [f.replace(dir_data, ''), params['date'], params['species'],
                     params['temperature'], params['viscosity']]
    file_index = os.path.join(dir_data, 'index.csv')
    df.to_csv(file_index)

def get_parameters_of_traj_file(fname):
    folder = os.path.split(fname)[0]
    param_file = os.path.join(folder, 'parameters.json')
    with open(param_file, 'rt') as file_handle:
        params = json.load(file_handle)
    return params


path_to_data_folder = '../data/'
create_index(path_to_data_folder)
