from lib_xy import file_io
from lib_xy.analysis import get_displacements

index = file_io.load_index('../data')


index_selected = index[(index['species'] == 'C') &
                       (index['viscosity'] == 1.0) &
                       (index['temperature'] == 1.4)]

for fname_traj in index_selected['file']:
    get_displacements(fname_traj)
