import numpy as np
import matplotlib.pyplot as plt

plt.style.use('poster')

def plot_time_consumed():
    fig, ax = plt.subplots(1, 1, figsize=(4, 3))

    ax.set(xlabel='number of repetitions',
           ylabel='time consumed',
           xticks=[],
           yticks=[])

    x = np.linspace(0, 10, 100)

    y_normal = x * 30 + 20
    y_automated = x * 12 + 85
    
    ax.plot(x, y_normal, label='low automization')
    ax.plot(x, y_automated, label='high automization')
    ax.legend()
    
    ylim = ax.get_ylim()
    ax.set_ylim(0, ylim[1])

    fig.savefig('time_spent_coding.svg')


def plot_time_spent_coding():
    fig, ax = plt.subplots(1, 1, figsize=(4, 3))

    
    x = np.linspace(0, 10, 100)

    y_normal = x * 30 + 20
    y_automated = x * 12 + 85
    y_normal_coding = x * 5 + 10
    y_automated_coding = x * 2.0 + 70

    ax.plot(x, y_normal, label='low automization')
    ax.plot(x, y_automated, label='high automization')
    ax.plot(x, y_normal_coding, '--C0')
    ax.plot(x, y_automated_coding, '--C1')
    ax.legend()
    ax.set(xlabel='number of repetitions',
           ylabel='time consumed',
           xticks=[],
           yticks=[])
    ylim = ax.get_ylim()
    ax.set_ylim(0, ylim[1])


if __name__ == "__main__":

    plot_time_consumed()
    plot_time_spent_coding()
    plt.show()
