.. _ExampleFolder:

Example Folder
==============

You can :download:`download the example files here </_static/project_xy.zip>`.
It's supposed to be the folder of a project xy, containing all files
you have for that project.

On the top level you have 3 folders:

.. code:: bash

   project_xy/
   ├── analysis
   ├── data
   └── lib_xy

Raw Data
--------

``data`` contains only raw data files and meta data in the beginning. Later on,
we will also generate intermediate result files next to the raw data files.
I like to create several subfolders for my data, structured into something
like chapters or categories. Those folders usually begin with a sequential,
zero-padded integer followed by some label, in this case:

.. code:: bash

   project_xy/data/
   ├── 00_species_A
   ├── 01_species_B
   └── 02_species_C

How you structure it is up to your taste, but I recommend to definitely do it
in a regular manner, to make automatic file finding not unnecessarily complicated.

Every subfolder like ``00_species_A`` has one folder for each experiment. Each experiment
folder has one or more trajectory recordings in this example:

.. code:: bash

   project_xy/data/00_species_A/
   ├── experiment_00
   │   ├── parameters.json
   │   ├── traj_00.dat
   │   ├── traj_01.dat
   │   ├── traj_02.dat
   │   └── traj_03.dat
   ├── experiment_01
   │   ├── parameters.json
   │   ├── traj_00.dat
   │   ├── traj_01.dat
   │   ├── traj_02.dat
   │   └── traj_03.dat
   └── experiment_02
       ├── parameters.json
       ├── traj_00.dat
       ├── traj_01.dat
       └── traj_02.dat

The ``traj_ii.dat`` files are text files with x and y positions of the particle at each frame.

Furthermore, there are parameter or meta data files called ``parameter.json`` in each
experiment folder.
I like to use JSON files since they are human readable and there are good libraries in every language for reading
and writing those files. So you can hand your files to any person and most likely they will have
a library for reading the meta data files in their favorite programming language.
JSON means **J**\ ava\ **S**\ cript **O**\ bject **N**\ otation, so it originates from JavaScript.
But the syntax is very similar to python dictionaries.

Library
-------

The folder ``lib_xy`` is the place for the code you need again and again in the analysis
for your ``project_xy``. For that recurrent code it's a bad idea to copy-paste it to where
you need it. You will get confused which is the latest version,
and you might use buggy old code in some scripts without knowing it. Thus, I recommend to
keep a centralized code library. This also allows you to leverage version control software
like `git <https://git-scm.com/>`_, which helps you a lot in keeping your code nice and tidy.
Give it a try!

I placed an ``__init__.py`` file in it, to make it a proper
python package. It means you can import python files from that folder. E.g. if
you have a file ``compute_msd.py`` in there that defines a variable ``x`` or function ``f``,
you can import them like this:

.. code:: python

   from lib_xy.compute_msd import f, x
   print(x)
   # or maybe you want to rather use them as compute_msd.f and compute_msd.x,
   # because those names x and f are ambiguous. In that case do:
   from lib_xy import compute_msd
   print(compute_msd.x)

For that to work from everywhere on your computer, you have to add the location
of the folder ``project_xy`` to your python path, otherwise your library
``lib_xy`` can't be found by your python scripts. A simple way to add the folder
to your python path is to create a text file containing only one line, the
path to ``project_xy``, in a the so called ``site-packages`` folder.
Figure out where your ``site-package`` folder is by running the following
lines of code in python:

.. code::

   import site
   print(site.USER_SITE)
   # if that does not work in your python version, try:
   # print(site.getusersitepackages())

This will point you to the ``site-pacakges`` folder, e.g. on my computer
``'/home/ilyas/.local/lib/python3.8/site-packages'``. In that folder, create
a text file with extension ``.pth`` (e.g. ``my_lib_xy.pth``) containing the path to your copy of ``project_xy``, e.g.

.. code:: bash

   /home/ilyas/projects/project_xy

When you save the file, you should be able to import ``lib_xy`` from anywhere on your computer.


Results and Scripts
-------------------

I created a folder named ``analisys``. I like to put all analysis scripts into that folder.
If they produce results that involve data from multiple folders, I write them into
the ``analysis`` folder or subfolders thereof. If the results are based on single
data files or data files from the same folder, I sometimes rather place the results
next to the data files.
