from multiprocessing import Pool
import time
import numpy as np

def long_analysis(idx):
    print("performing long analysis {:02} ...".format(idx))
    time.sleep(2)
    
def perform_analysis_parallel(indices, n_proc):
    print("start analysis in parallel")
    t_start = time.time()
    # prepare multiprocessing with n_proc number of processes    
    pool = Pool(n_proc) 
    pool.map(long_analysis, indices)
    print("finished in {:.2f} seconds".format(time.time() - t_start))

def perform_analysis_sequential(indices):
    print("start analysis sequentially")
    t_start = time.time()
    for i in indices:
        long_analysis(i)
    print("finished in {:.2f} seconds".format(time.time() - t_start))
        

if __name__ == "__main__":

    indices = np.arange(10)
    perform_analysis_sequential(indices)
    perform_analysis_parallel(indices, 4)
    
