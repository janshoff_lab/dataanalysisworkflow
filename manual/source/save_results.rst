.. _SaveResults:

Save and Load Intermediate Results
==================================

Sometimes you have very time consuming analyses
(e.g. extracting trajectories of single particles from videos, performing
an elaborate fit, etc.). In that case
it's often worth it to save intermediate results, and compute
an update only if any data files were added or updated. And if you compute
intermediate results separately
for each raw data file, you probably want to do the time consuming analysis
only for the new files, while loading the intermediate results for old files.

In this section I will give you an example how to do that with Python. Depending
on your taste and the amount of data, you can choose to save intermediate results
in separate files (text files or maybe numpy files). If you have a lot of data or prefer
not to create hundreds of additional files for aesthetic reasons, you should
consider using `HDF5 <https://www.hdfgroup.org/>`_ files.

In this small artificial example, storing intermediate results is not necessary at all,
maybe even disadvantageous. This is just to illustrate how to save and load those results.
We will **compute displacements** for each trajectory, and **save those displacements** for each
trajectory. We will then **load them again** to produce **MSD plots** and **van-Hove plots**.

General Concept
---------------

I like to define functions that start with ``compute`` and ``get`` for a property:

.. code::

   def compute_displacements(file_traj):
       ...

   def get_displacements(file_traj):
       ...

The ``compute`` functions do the actual work of computing results, and return an array.
The ``get`` functions will rather load the results from file, if they exist, and return
them. If they
do not exist, they are responsible for calling the ``compute`` function, save the results,
and then return them.

Since most ``get`` functions work in a very similar manner, they internally call the same
``get_property`` function with different arguments.

So the ``get_property`` function will be different for both sections below, but the
``get_displacements`` and ``compute_displacements`` functions will be identical:

.. code::

   def get_displacements(fname_traj):
       return get_property(fname_traj, 'displacements',
                           compute_displacements)
   
   def compute_displacements(fname_traj):
       traj = np.genfromtxt(fname_traj, delimiter=',')
       displacements = []
       for i in range(1, len(traj)):
           displacements_i = np.empty((len(traj)-i, 3))
           # save current step size in first column
           displacements_i[:, 0] = i
           # displacements in 2nd and 3rd column
           displacements_i[:, 1:] = traj[i:] - traj[:-1] 
           displacements.append(displacements_i)
       return np.vstack(displacements)
   

Via Separate Files
------------------

We want to store intermediate results for each raw data file. I would recommend
using a separate folder named as the file, but with file extension removed and
``_results`` appended. So e.g. file ``project_xy/data/01_species_B/experiment_00/traj_02.dat``
has its results saved in folder ``project_xy/data/01_species_B/experiment_00/traj_02_results``.
Each result file will simply be named by its label, e.g. ``displacements.npy``.
This is achieved by the following function:

.. code::

   def get_property(fname_traj, prop, compute_function):
       fname_root = os.path.splitext(fname_traj)[0]
       results_dir = fname_root + '_results'
       os.makedirs(results_dir, exist_ok=True)
       fname_prop = os.path.join(results_dir, '{}.npy'.format(prop))
       if os.path.exists(fname_prop):
           # load from file
           return np.load(fname_prop)        
       # compute
       results = compute_function(fname_traj)
       # and save to file
       np.save(fname_prop, results)
       return results
   

Via HDF5 Files
--------------

I prefer to store results in HDF5 files, you can put everything in one big file,
instead creating hundreds of separate ones. The downside: you can't just use
your file browser to check whether the file was created. You would have to
do that with a few lines of code.

In this approach, instead of a folder for each raw data file, I would create a file
with ``_results.h5`` appended, e.g.
``project_xy/data/01_species_B/experiment_00/traj_02_results.h5``.
I named the function ``get_property_h5`` here, so you can try both styles at the
same time. In real scenarios, you will probably decide for one style and stick with it.

.. code::

   # requires package h5py!

   def get_property_h5(fname_traj, prop, compute_function):
       fname_root = os.path.splitext(fname_traj)[0]
       fname_results = fname_root + '_results.h5'
       h5_file = h5py.File(fname_results, 'a') # 'a' is for read/write/create if not existing 
       if prop in h5_file:
           # load from file
           # access datasets with h5_file[name]
           # the last indexing [:] is to make it a numpy array
           return h5_file[prop][:]
       # not found in file -> compute
       results = compute_function(fname_traj)
       h5_file[prop] = results
       h5_file.close()
       return results
   
Let's Precompute Displacements for a few Files
----------------------------------------------

I put all of those functions in ``project_xy/lib_xy/analysis.py``:

.. code-block::
   :caption: analysis.py


   def get_displacements(fname_traj):
       # change to get_property_h5
       # if you want the HDF5 version
       return get_property(fname_traj, 'displacements',
                           compute_displacements)
   
   def compute_displacements(fname_traj):
       traj = np.genfromtxt(fname_traj, delimiter=',')
       displacements = []
       for i in range(1, len(traj)):
           displacements_i = np.empty((len(traj)-i, 3))
           # save current step size in first column
           displacements_i[:, 0] = i
           # displacements in 2nd and 3rd column
           displacements_i[:, 1:] = traj[i:] - traj[:-i] 
           displacements.append(displacements_i)
       return np.vstack(displacements)
   
   def get_property(fname_traj, prop, compute_function):
       fname_root = os.path.splitext(fname_traj)[0]
       results_dir = fname_root + '_results'
       os.makedirs(results_dir, exist_ok=True)
       fname_prop = os.path.join(results_dir, '{}.npy'.format(prop))
       if os.path.exists(fname_prop):
           print("loading {} for file {}".format(prop, fname_traj))
           # load from file
           return np.load(fname_prop)        
       # not found in file -> compute
       print("computing {} for file {}".format(prop, fname_traj))
       results = compute_function(fname_traj)
       # and save to file
       np.save(fname_prop, results)
       return results
   
   def get_property_h5(fname_traj, prop, compute_function):
       fname_root = os.path.splitext(fname_traj)[0]
       fname_results = fname_root + '_results.h5'
       h5_file = h5py.File(fname_results, 'a') # 'a' is for read/write/create if not existing 
       if prop in h5_file:
           # load from file
           # access datasets with h5_file[name]
           # the last indexing [:] is to make it a numpy array
           return h5_file[prop][:]
       # not found in file -> compute
       results = compute_function(fname_traj)
       h5_file[prop] = results
       h5_file.close()
       return results
      
I inserted a few status messages, to see whether the displacements were loaded
or computet. In the beginning, they should all get computet.

Let's run the displacements computation for a few files. I put a script
``precompute_displacements.py`` into ``project_xy/analysis/``.
It will call ``get_displacements`` for all files with
``species == 'C' and viscosity == 1.0 and temperature == 1.4``:

.. code-block::
   :name: precompute_displacements.py

   from lib_xy import file_io
   from lib_xy.analysis import get_displacements
   
   index = file_io.load_index('../data')
   
   
   index_selected = index[(index['species'] == 'C') &
                          (index['viscosity'] == 1.0) &
                          (index['temperature'] == 1.4)]
   
   for fname_traj in index_selected['file']:
       get_displacements(fname_traj)
   

Load/compute Displacements for Van Hove Plots
---------------------------------------------

Now let's create Van Hove plots. For that we will use files for all species
with ``viscosity == 1.0`` and ``temperature == 1.4``. I put the script
for the plot into ``project_xy/analysis/vanhove/create_plots.py``:

.. code::
   :name: project_xy/analysis/vanhove/create_plots.py

   import matplotlib.pyplot as plt
   from lib_xy import file_io
   from lib_xy.analysis import plot_vanhove
   
   visc = 1.0
   temp = 1.4
   lag  = 5
   
   fig, ax = plt.subplots(2, 1, sharex=True)
   ax_x = ax[0]
   ax_y = ax[1]
   
   # load index
   index = file_io.load_index('../../data')
   # select files of all species with matching visc. and temp.
   index_selected = index[(index['viscosity'] == visc) &
                          (index['temperature'] == temp)]
   
   # iterate over all species
   for species in sorted(index_selected['species'].unique()):
       # select only matching species
       index_species = index_selected[index_selected['species'] == species]
       # for the plot we need only the file column
       files = index_species['file']
       plot_vanhove(ax_x, ax_y, files, lag, {'label': species, 'bins': 50,
                                             'alpha': 0.8})
   
   
   fig.suptitle('visc. = {:.2f}; temp. = {:.2f}; lag = {}'.format(
       visc, temp, lag))
   ax_x.legend()
   ax_x.set(ylabel='density',
            xlabel='displacement in x')
   ax_y.set(ylabel='density',
            xlabel='displacement in y')

   fig.savefig("vanhove_nu_{:.2f}_T_{:.2f}_lag_{}.png".format(visc, temp, lag))
   fig.savefig("vanhove_nu_{:.2f}_T_{:.2f}_lag_{}.svg".format(visc, temp, lag))
   fig.show()
   
Run it to produce a plot like this:

.. image:: /_static/vanhove.svg
   :width: 100%

The function loaded from ``lib_xy.analysis`` is the following:

.. code::

   def plot_vanhove(ax_x, ax_y, traj_files, lag, plot_kwargs):
       displacements = []
       for f in traj_files:
           displ_f = get_displacements(f)
           displacements.append(displ_f[displ_f[:, 0] == lag, 1:])
       displacements = np.vstack(displacements)
       kwargs = {'density': True}
       kwargs.update(plot_kwargs)
       ax_x.hist(displacements[:, 0], **kwargs)
       ax_y.hist(displacements[:, 1], **kwargs)
   

Exercises
---------

Since I didn't have time to include the following things, I'm gonna put them here as exercises
without any solutions. If you need anything similar to these exercises in your data analysis,
you can ask me for help, and we can work on a suitable system together.
If you are interested in solutions to exercises, you can request them from me, and I
will include them here. :)

MSD
~~~

For all species, compute and plot mean square displacements for fixed viscosity and temperature.
As for the Van Hove plot, include a function in ``project_xy/lib_xy/analysis.py`` for that, in
which you get the displacements via the function ``get_displacements``.

Bonus
+++++

Since you have to combine data of a lot of files for the MSDs, it makes sense to save the
final results for MSDs as well: the mean and the standard deviation of the MSD values.
Since you compute them separately by species, viscosity and temperature, you should store them also
in separate files or HDF5 datasets. String formatting comes in handy for that, e.g.
``"msd_species_{}_visc_{:.2f}_temp_{:.2f}".format(species, viscosity, temperature)``.
This kind of combined intermediate result (data merged from multiple raw data files) should be saved in the
folder ``project_xy/analysis``.
You also have to use a different kind of ``get_property`` function here, that accounts for the parameters!

Timestamp comparison in ``get_property``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes you discover a bug in one of the analysis functions that produce an intermediate results.
If further analyses depend on that result, you currently have to make sure manually, that you
take the updated result into account. For that it would be helpful,
to include a check in the ``get_property`` function:

* If the timestamp of any file the analysis depends on is newer than
  the timestamp of the saved analysis results, it needs to compute those
  dependencies again.
  
* Otherwise you can safely use the saved result.
