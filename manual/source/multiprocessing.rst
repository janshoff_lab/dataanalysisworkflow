.. _Multiprocessing:

Multiprocessing
===============

With multiprocessing you can run parts of your analysis in parallel. In the ideal case,
you get a speed-up scaling almost linearly with the number of your computer's threads/processes
(which is something around 4-8 for most computers, but 48 or more for our avalon clusters,
and when they are free you could also use those). In many cases, multiprocessing is not suitable
at all. It can be hard to judge whether you can benefit from it. **But!** If you run a long
analysis on a per-file basis (and I find myself often in that situation) you can almost blindly
apply it and speed up your analysis. If you run a for loop over files, and each
loop uses only data from that one file, you might as well run them in parallel with multiprocessing.

Let's say you are in a situation where you have a list/array of file names, and you want to pass
them to a function in a for loop:

.. code::

   results = []
   # files = some list or array of files
   for f in files:
       result_f = perform_some_analysis(f)
       results.append(result_f)

In that scenario it's very easy to use multiprocessing instead:

.. code::

   from multiprocessing import Pool
   
   n_processes = 4
   pool = Pool(n_processes)
   results = pool.map(perform_some_analysis, files)


I put a toy demo into ``project_xy/analysis/multiprocessing_demo.py``, even
though it has nothing to do with our project_xy. The analysis function
is just a dummy. The short analysis for displacement computation does not benefit
from multiprocessing. Here's the file:

.. code::
   :name: project_xy/analysis/multiprocessing_demo.py

   from multiprocessing import Pool
   import time
   import numpy as np
   
   def long_analysis(idx):
       print("performing long analysis {:02} ...".format(idx))
       time.sleep(2)
       
   def perform_analysis_parallel(indices, n_proc):
       print("start analysis in parallel")
       t_start = time.time()
       # prepare multiprocessing with n_proc number of processes       
       pool = Pool(n_proc) 
       pool.map(long_analysis, indices)
       print("finished in {:.2f} seconds".format(time.time() - t_start))
   
   def perform_analysis_sequential(indices):
       print("start analysis sequentially")
       t_start = time.time()
       for i in indices:
           long_analysis(i)
       print("finished in {:.2f} seconds".format(time.time() - t_start))
           
   
   if __name__ == "__main__":
   
       indices = np.arange(10)
       perform_analysis_sequential(indices)
       perform_analysis_parallel(indices, 4)
       

   
