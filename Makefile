all:
	mkdir -p build/project_xy/analysis/MSD
	mkdir -p build/project_xy/lib_xy
	touch build/project_xy/lib_xy/__init__.py
	cp -r example/project_xy/library/* build/project_xy/lib_xy/
	cp -r example/project_xy/analysis/* build/project_xy/analysis/
	python generate_example_data.py build/project_xy
	cd build; zip -r project_xy.zip project_xy
	cp build/project_xy.zip manual/source/_static/project_xy.zip
	$(MAKE) -C manual html
	mkdir -p build/manual		
	cp -r manual/build/html/* build/manual
	cp redirect_to_index.html build/manual.html

clean:
	$(MAKE) -C manual clean
	rm -r build

