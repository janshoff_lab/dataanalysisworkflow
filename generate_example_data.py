import os
import sys
import json
import numpy as np
import datetime

dt = 0.01

def generate_trajectory(n, dt, kT, eta, r):
    D = kT/6/np.pi/eta/r
    dxy = np.sqrt(2*D*dt)*np.random.randn(n-1, 2)
    xy = np.empty((n, 2))
    xy[0] = [0, 0]
    xy[1:] = np.cumsum(dxy, 0)
    return xy

def populate_subfolder(dirname, species, r, n_trajectories,
                       temperatures, viscosities, dates):
    os.makedirs(dirname, exist_ok=True)
    for i, n in enumerate(n_trajectories):
        # create experiment folder
        d = dirname + "/experiment_{:02}".format(i)
        os.makedirs(d, exist_ok=True)
        
        # create parameter file
        with open(d + '/parameters.json', 'wt') as fh:
            params = {'temperature': temperatures[i],
                      'viscosity': viscosities[i],
                      'species': species,
                      'date': str(dates[i])}
            json.dump(params, fh)
            
        # create trajectory files
        for j in range(n):
            n_steps = int(750 + np.random.randn()*200)
            xy = generate_trajectory(n_steps, dt, temperatures[i],
                                     viscosities[i], r)
            write_traj(d + '/traj_{:02}.dat'.format(j), xy)

def write_traj(fname, xy):
    with open(fname, 'wt') as fh:
        fh.write("# x, y\n")
        for xyi in xy:
            fh.write("{}, {}\n".format(*xyi))

    

if __name__ == "__main__":
    if len(sys.argv) < 2:
        raise RuntimeError("argument path to root dir missing!")
    np.random.seed(20191122)
    root = sys.argv[1]
    os.makedirs(root, exist_ok=True)
    dir_data = root + "/data"
    os.makedirs(dir_data, exist_ok=True)

    temperature_set = [1.0, 1.2, 1.4]
    viscosities_set = [1.0, 2.0]

    species = {'A': 1.0,
               'B': 15.0,
               'C': 5.0}

    for i, sp in enumerate(species):
        dir_species = dir_data + "/{:02}_species_{}".format(i, sp)
        n_experiments = np.random.randint(10) + 5
        n_trajectories = np.random.randint(1, 10, size=n_experiments)
        temperatures = np.random.choice(temperature_set, n_experiments)
        viscosities = np.random.choice(viscosities_set, n_experiments)
        dates = np.random.choice(np.arange(10, 30)*80000, n_experiments)
        dates = np.cumsum(dates)+1.3e9
        dates = [datetime.datetime.fromtimestamp(d) for d in dates]
        populate_subfolder(dir_species, sp, species[sp], n_trajectories,
                           temperatures, viscosities, dates)
