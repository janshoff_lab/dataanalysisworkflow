.. _FindingFiles:

Finding Files
=============

The best tool to find files in Python is the package ``glob``.
It's part of Python's standard packages, so it should be available if you
have Python installed. We can easily find all data files with:

.. code::

   path_to_data_folder = 'put/path/to/folder/project_xy/data'
   files_traj = sorted(glob.glob(path_to_data_folder + '/**/traj*.dat',
                                 recursive=True))
   for f in files_traj:
       print(f)

Make sure you insert the right path to the data folder. E.g., when you are
currently in ``project_xy/analysis`` in your terminal, you can set
``path_to_data_folder = '../data'``.


File Index
==========

I'm a fan of combining data files and meta data in a file index,
to make selection of files by parameters very easy, e.g.
to select all files of experiments with ``viscosity = 0.5`` (arbitrary units ...)
or recorded after 2nd of August 2016.

A very handy tool for that purpose is a pandas ``DataFrame``. Pandas is a popular package
for Python to make working with tables easy. In contrast to numpy, it's more comfortable
for arrays with different data types.

Let's create such a file index in a pandas ``DataFrame``. In this example, the parameter
files have 4 values:

* ``temperature``: a floating point number
* ``viscosity``: a floating point number
* ``species``: a string
* ``date``: a string containing a date and a time

Additionaly, we want a 5th column in our table to hold the path to a file, which will
also be a string. Prepare the ``DataFrame`` like this:

.. code::

   import pandas as pd
   df = pd.DataFrame(columns=['file', 'date', 'species',
                              'temperature', 'viscosity'])

To fill it, we want to get the parameters of every file. The file entries in our list
``files_traj`` from the last section look e.g. like this:

.. code::

   '../data/02_species_C/experiment_04/traj_03.dat'

Nice functions exist in the package ``os.path`` to work with that kind of strings.
To get the folder of the file, we can use:

.. code::

   import os
   ftraj = '../data/02_species_C/experiment_04/traj_03.dat'
   folder, filename = os.path.split(file_traj)

The variable ``folder`` now contains only the string ``'../data/02_species_C/experiment_04``
without the actual name of the file. Since the layout of our data folders is regular, there
always is a ``parameters.json`` file in that folder. We can use another function from ``os.path``
to safely construct the path to that file:

.. code::

   file_params = os.path.join(folder, 'parameters.json')

Read the contents of that file into a dictionary:

.. code::

   import json
   with open(file_params, 'rt') as file_handle:
       params = json.load(file_handle)

The ``with`` statement makes sure, that the file is closed again after we leave the
indentation level of the ``with`` statement. The second parameters ``'rt'`` tells
Python to open the file in read-text-mode.
``params`` is now a dictionary holding the key-value-pairs from the json file.

To write that to line ``i`` of the ``DataFrame``, do:

.. code::

   df.loc[i] = [ftraj, params['date'], params['species'],
                params['temperature'], params['viscosity']]

To access a line, you need to use ``df.loc`` followed by an index in square brackets.
To access columns, you would leave out the ``.loc``. E.g. to get the file column,
type ``df['file']``.

I put the code of the last two section together in ``project_xy/analysis/create_index.py``.

.. code::

   import os
   import json
   import pandas as pd
   from lib_xy import file_io
   
   
   def create_index(dir_data):
       df = pd.DataFrame(columns=['file', 'date', 'species', 'temperature', 'viscosity'])
       files_traj = file_io.find_traj_files(dir_data)
       for i, f in enumerate(files_traj):
           params = get_parameters_of_traj_file(f)
           df.loc[i] = [f.replace(dir_data, ''), params['date'], params['species'],
                        params['temperature'], params['viscosity']]
       file_index = os.path.join(dir_data, 'index.csv')
       df.to_csv(file_index)
   
   def get_parameters_of_traj_file(fname):
       folder = os.path.split(fname)[0]
       param_file = os.path.join(folder, 'parameters.json')
       with open(param_file, 'rt') as file_handle:
           params = json.load(file_handle)
       return params
   
   
   path_to_data_folder = '../data/'
   create_index(path_to_data_folder)
      
Run it to create the file ``project_xy/data/index.csv``. You can also 
view and change it in in a table calculation program like Microsoft Excel,
libreoffice calc, etc.

Load Index
----------

I added a function ``load_index`` to ``file_io``. Your scripts should
now always start with that:

.. code::

   from lib_xy import file_io

   path_to_data = '../data'
   index = file_io.load_index(path_to_data)

   # now you can select all lines with species B:
   index_sp_B = index[index['species'] == 'B']
   # in a next step, maybe you only want to iterate over viscosities:
   for v in index_sp_B['viscosity'].unique():
       index_sp_B_v = index_sp_B[index_sp_B['viscosity'] == v]
       print('-'*80)
       print('viscosity:', v)
       print(index_sp_B_v)
   
