import os
import numpy as np
import h5py

def get_displacements(fname_traj):
    # change to get_property_h5
    # if you want the HDF5 version
    return get_property(fname_traj, 'displacements',
                        compute_displacements)

def compute_displacements(fname_traj):
    traj = np.genfromtxt(fname_traj, delimiter=',')
    displacements = []
    for i in range(1, len(traj)):
        displacements_i = np.empty((len(traj)-i, 3))
        # save current step size in first column
        displacements_i[:, 0] = i
        # displacements in 2nd and 3rd column
        displacements_i[:, 1:] = traj[i:] - traj[:-i] 
        displacements.append(displacements_i)
    return np.vstack(displacements)

def get_property(fname_traj, prop, compute_function):
    fname_root = os.path.splitext(fname_traj)[0]
    results_dir = fname_root + '_results'
    os.makedirs(results_dir, exist_ok=True)
    fname_prop = os.path.join(results_dir, '{}.npy'.format(prop))
    if os.path.exists(fname_prop):
        print("loading {} for file {}".format(prop, fname_traj))
        # load from file
        return np.load(fname_prop)        
    # not found in file -> compute
    print("computing {} for file {}".format(prop, fname_traj))
    results = compute_function(fname_traj)
    # and save to file
    np.save(fname_prop, results)
    return results

def get_property_h5(fname_traj, prop, compute_function):
    fname_root = os.path.splitext(fname_traj)[0]
    fname_results = fname_root + '_results.h5'
    h5_file = h5py.File(fname_results, 'a') # 'a' is for read/write/create if not existing 
    if prop in h5_file:
        # load from file
        # access datasets with h5_file[name]
        # the last indexing [:] is to make it a numpy array
        return h5_file[prop][:]
    # not found in file -> compute
    results = compute_function(fname_traj)
    h5_file[prop] = results
    h5_file.close()
    return results
    
def plot_vanhove(ax_x, ax_y, traj_files, lag, plot_kwargs):
    displacements = []
    for f in traj_files:
        displ_f = get_displacements(f)
        displacements.append(displ_f[displ_f[:, 0] == lag, 1:])
    displacements = np.vstack(displacements)
    kwargs = {'density': True}
    kwargs.update(plot_kwargs)
    ax_x.hist(displacements[:, 0], **kwargs)
    ax_y.hist(displacements[:, 1], **kwargs)
                                     
