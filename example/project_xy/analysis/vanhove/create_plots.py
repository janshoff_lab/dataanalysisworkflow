import matplotlib.pyplot as plt
from lib_xy import file_io
from lib_xy.analysis import plot_vanhove

visc = 1.0
temp = 1.4
lag  = 5

fig, ax = plt.subplots(2, 1, sharex=True)
ax_x = ax[0]
ax_y = ax[1]

# load index
index = file_io.load_index('../../data')
# select files of all species with matching visc. and temp.
index_selected = index[(index['viscosity'] == visc) &
                       (index['temperature'] == temp)]

# iterate over all species
for species in sorted(index_selected['species'].unique()):
    # select only matching species
    index_species = index_selected[index_selected['species'] == species]
    # for the plot we need only the file column
    files = index_species['file']
    plot_vanhove(ax_x, ax_y, files, lag, {'label': species, 'bins': 50,
                                          'alpha': 0.8})


fig.suptitle('visc. = {:.2f}; temp. = {:.2f}; lag = {}'.format(
    visc, temp, lag))
ax_x.legend()
ax_x.set(ylabel='density',
         xlabel='displacement in x')
ax_y.set(ylabel='density',
         xlabel='displacement in y')

fig.savefig("vanhove_nu_{:.2f}_T_{:.2f}_lag_{}.png".format(visc, temp, lag))
fig.savefig("vanhove_nu_{:.2f}_T_{:.2f}_lag_{}.svg".format(visc, temp, lag))
fig.show()
